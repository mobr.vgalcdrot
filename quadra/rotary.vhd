
library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity rotary is
   port (
      AD       : out std_logic_vector (31 downto 0);
      clk      : in  std_logic;
      ROTARY   : in  std_logic_vector (2 downto 0); -- rotary button  (left, right, push)
      REGX     : out std_logic_vector (7 downto 0); -- X
      REGY     : out std_logic_vector (7 downto 0)  -- Y
   );
end rotary;

architecture rtl of rotary is
	signal ROT_D : std_logic_vector (2 downto 0);
	signal ra, rb, rc, rc_d : std_logic;
	signal rx, ry : std_logic_vector (7 downto 0);
	type QSTATE is ( DET, A1, A2, A3, B1, B2, B3 );
	signal state, next_state : QSTATE;
	signal inc, dec, inc_d, dec_d : std_logic;
	signal reg_sel : std_logic;

   component debounce
		port (
			clk, tin : in std_logic;
			tout : out std_logic
		);
	end component;

begin
	-- vystup do AD
	AD <= rx & x"0000" & ry;

	-- vystup do REGX, REGY
	REGX <= rx;
	REGY <= ry;

	-- vlozi extra D na vstup
	delayinputpr: process (clk)
	begin
		if clk'event and clk = '1' then
			ROT_D <= ROTARY;
			ra <= ROTARY(2);
			rb <= ROTARY(1);
			rc <= ROTARY(0);
		end if;
	end process delayinputpr;

	-- prepinani registru
	-- nema debounce !!!
	sigselpr: process (clk)
	begin
		if clk'event and clk = '1' then
			if rc_d = '1' then
				if reg_sel = '1' then
					reg_sel <= '0';
				else
					reg_sel <= '1';
				end if;
			end if;
		end if;
	end process sigselpr;

	-- inkrementuje nebo dekrementuje zvoleny registr
	-- v zavislosti na signalu inc, dec
	-- aktivni muze byt jen jeden z nich
	regincpr: process (clk)
	begin
		if clk'event and clk = '1' then
			if reg_sel = '1' then
				if inc = '1' then
					ry <= ry + 1;
				elsif dec = '1' then
					ry <= ry - 1;
				end if;
			else
				if inc = '1' then
					rx <= rx + 1;
				elsif dec = '1' then
					rx <= rx - 1;
				end if;
			end if;
		end if;
	end process regincpr;

-----------------------------------------
-- automat
-----------------------------------------

	-- nasledujici stav automatu
	nextstatepr: process (ra, rb, state)
	begin
		-- defaultne nemen stav
		next_state <= state;
		if ra = '0' and rb = '0' then
			-- pokud je cudlik detentovan, jdi do nuloveho stavu
			-- dobre pro superrychle otaceni cudliku (joke)
			-- nebo pro autoreset na zacatku
			next_state <= DET;
		end if;
		case STATE is
			when DET =>
				if ra = '1' and rb = '0' then
					next_state <= A1;
				elsif ra = '0' and rb = '1' then
					next_state <= B3;
				end if;
			when A1 =>
				if ra = '1' and rb = '1' then
					next_state <= A2;
				elsif ra = '0' and rb = '0' then
					next_state <= DET;
				end if;
			when A2 =>
				if ra = '1' and rb = '0' then
					next_state <= A1;
				elsif ra = '0' and rb = '1' then
					next_state <= A3;
				end if;
			when A3 =>
				if ra = '1' and rb = '1' then
					next_state <= A2;
				elsif ra = '0' and rb = '0' then
					next_state <= DET;
				end if;
			when B1 =>
				if ra = '0' and rb = '0' then
					next_state <= DET;
				elsif ra = '1' and rb = '1' then
					next_state <= B2;
				end if;
			when B2 =>
				if ra = '1' and rb = '0' then
					next_state <= B1;
				elsif ra = '0' and rb = '1' then
					next_state <= B3;
				end if;
			when B3 =>
				if ra = '0' and rb = '0' then
					next_state <= DET;
				elsif ra = '1' and rb = '1' then
					next_state <= B2;
				end if;
		end case;
	end process nextstatepr;

	-- prechod do dalsiho stavu
	-- take zpozdi vystup automatu o jeden takt
	-- to z nej efektivne dela moore
	statechangepr: process (clk)
	begin
		if clk'event and clk = '1' then
			state <= next_state;
			-- zpozdeni ridicich signalu
			-- ani neni treba, neb ra a rb uz jsou
			-- synchronizovany hodinami
			inc <= inc_d;
			dec <= dec_d;
		end if;
	end process statechangepr;

	-- vystupni funkce automatu (mealy)
	outputfunpr: process (ra, rb, state)
	begin
		inc_d <= '0';
		dec_d <= '0';
		case STATE is
			when DET => null;
			when A1 => null;
			when A2 => null;
			when A3 =>
				if ra = '0' and rb = '0' then
					inc_d <= '1';
				end if;
			when B1 =>
				if ra = '0' and rb = '0' then
					dec_d <= '1';
				end if;
			when B2 => null;
			when B3 => null;
		end case;
	end process outputfunpr;

-----------------------------------------
-- /automat
-----------------------------------------

-----------
-- debounce
-----------
	db : debounce port map (clk => clk, tin => rc, tout => rc_d);


end architecture rtl;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:09:48 12/05/2011 
-- Design Name: 
-- Module Name:    top - top_body 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
	port (
		clk : in std_logic;
		led : out std_logic_vector (7 downto 0);
		rota : in std_logic_vector (2 downto 0)
	);
end top;

architecture top_body of top is
	
	component rotary
		port (
			AD       : out std_logic_vector (31 downto 0);
			clk      : in  std_logic;
			ROTARY   : in  std_logic_vector (2 downto 0); -- rotary button  (left, right, push)
			REGX     : out std_logic_vector (7 downto 0); -- X
			REGY     : out std_logic_vector (7 downto 0)  -- Y
		);
	end component;
	signal AD : std_logic_vector (31 downto 0);
	signal reg : std_logic_vector (7 downto 0);
begin

	rot: rotary port map (
		AD => AD,
		clk => clk,
		ROTARY => rota,
		regx => led,
		regy => reg
	);

end top_body;


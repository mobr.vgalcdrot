----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:40:10 12/05/2011 
-- Design Name: 
-- Module Name:    debounce - debounce_body 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debounce is
	port (
		clk, tin : in std_logic;
		tout : out std_logic
	);
end debounce;

architecture debounce_body of debounce is
	signal cnt : std_logic_vector (19 downto 0);
	signal click : std_logic;
	signal last, prev : std_logic;
begin
	clkpr: process (clk)
	begin
		if clk'event and clk = '1' then
			click <= '0';
			cnt <= cnt + 1;
			if cnt = 0 then
				click <= '1';
			end if;
		end if;
	end process clkpr;

	lastpr: process (clk)
	begin
		if clk'event and clk = '1' then
			if click = '1' then
				prev <= last;
				last <= tin;
			end if;
		end if;
	end process;
	
	tout <= click and (last and (not prev));

end debounce_body;


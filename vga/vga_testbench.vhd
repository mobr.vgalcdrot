
--------------------------------------------------------------------------------
-- Testbench k VGA
-- testbench k 1. casti 2. zadani 2. ulohy predmetu BI-PNO Z11.
--
-- Testuje spravnost VGA vystupu ve vsech pixelovych bodech.
-- Testuje nektere moznosti, ktere zobrazovac dokaze zobrazit.
-- Pozor, kontroluje se pouze kazde druhe vykresleni obrazovky.
-- Simulacni cas je neco pres 667 ms
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
package casovani is
	-- casovaci konstanty k VGA 640x480
    constant vts : time := 16.672 ms;
    constant vtdisp : time := 15.36 ms;
    constant vtpw : time := 64 us;
    constant vtfp : time := 320 us;
    constant vtbp : time := 928 us;
    constant hts : time := 32 us;
    constant htdisp : time := 25.6 us;
    constant htpw : time := 3.84 us;
    constant htfp : time := 640 ns;
    constant htbp : time := 1.92 us;
    constant tpix : time := 40 ns;
	constant tsm : time := 1 ns;
	-- funkce
	function time_to_first( x, y : integer ) return time;
	function expected_pixel (x, y, p : integer ) return std_logic_vector;
	procedure rnd_num ( variable num : out integer );
end package;

package body casovani is
	
	-- toto je neuzitecna funkce, jez neni pouzita
	-- z doby, kdy byl TB resen jinak
	-- jsem liny ji mazat
	-- zajimave ale je, ze psat tento komentar liny nejsem
	function time_to_first ( x, y : integer ) return time is
	begin
		-- vertikalni back porch, horixontalni back porch
		-- a x * pixeltime
		return vtbp + htbp + x * tpix;
	end function time_to_first;
	
	-- funkce vraci hodnotu ocekavanych vystupu na pixelu
	function expected_pixel (x, y, p : integer ) return std_logic_vector is
		variable px, py : integer;
	begin
		-- p je dvojnasobek pixelu, obrazovka zacina na 144. pixelu
		px := ((p mod 1600) / 2) - 144;
		-- obrazovka zacina na 31. radce;
		py := (p / 1600) - 31;
		if (px = x or py = y) and (px >= 0 and px < 640 and py >= 0 and py < 480) then
			return "111";
		else
			return "000";
		end if;
	end function expected_pixel;

	-- vraci nahodne cislo 0..255
	procedure rnd_num ( variable num : out integer ) is
		variable tmpa : positive := 1030;
		variable tmpb : positive := 1886;
		variable tmp : real;
	begin
		uniform(tmpa, tmpb, tmp);
		num := integer( trunc( tmp * 256.0 ) ) mod 256;
		return;
	end procedure rnd_num;

end package body;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
use work.casovani.all;

entity vga_testbench is
end vga_testbench;

architecture behavior of vga_testbench is 

  -- Component Declaration for the Unit Under Test (UUT)
  component vgs
    port(
      clk       : in  std_logic; -- 50 MHz              
      regX      : in  std_logic_vector (7 downto 0); -- X
      regY      : in  std_logic_vector (7 downto 0); -- Y
      VGA_RED   : out std_logic;                        
      VGA_GREEN : out std_logic;                        
      VGA_BLUE  : out std_logic;                        
      VGA_HSYNC : out std_logic;                        
      VGA_VSYNC : out std_logic 	
    );
  end component;

  signal clk       : std_logic; -- 50 MHz              
  -- vim, ze inicializace signalu pred testbenchem se nekterym lidem
  -- nelibi, ale nevim jak jinak se zbavit neprijemnych warningu
  -- v case 0, ktere nemaji zadny vyznam.
  signal regX      : std_logic_vector (7 downto 0) := x"00"; -- X
  signal regY      : std_logic_vector (7 downto 0) := x"00"; -- Y
  signal VGA_RED   : std_logic;                        
  signal VGA_GREEN : std_logic;                        
  signal VGA_BLUE  : std_logic;                        
  signal VGA_HSYNC : std_logic;                        
  signal VGA_VSYNC : std_logic; 
  -- cislo pulpixelu, na kterem jsme, pozor, ne pixelu!
  signal pixel     : integer := 0;

begin

  -- Instantiate the Unit Under Test (UUT)
  uut: vgs port map(
    clk        => clk       ,
    regX       => regX      ,
    regY       => regY      ,
    VGA_RED    => VGA_RED   ,
    VGA_GREEN  => VGA_GREEN ,
    VGA_BLUE   => VGA_BLUE  ,
    VGA_HSYNC  => VGA_HSYNC ,
    VGA_VSYNC  => VGA_VSYNC
  );

	-- generuje cislo pulpixelu na kterem prave jsme
	-- na zacatku se synchronizuje proti signalu VGA_VSYNC
	-- dale jiz bezi volne
	pixelpr: process
	begin
		pixel <= 0;
		wait until VGA_VSYNC = '0';
		loop
			wait until clk = '1';
			pixel <= pixel + 1;
			if pixel >= 416800 * 2 - 1 then
				pixel <= 0;
			end if;
		end loop;
	end process pixelpr;
  
	-- kontroluje tvar signalu VGA_VSYNC
	-- v pripade chyby ukonci simulaci
	-- protoze ostatni testy predpokladaji
	-- spravny tvar tohoto signalu, pri
	-- jeho chybe nema smysl pokracovat v simulaci
	-- protoze cislo pixelu bezi volne
	vsynctbpr: process
		variable tsa : time;
	begin
		-- pockat na VSYNC
		wait until VGA_VSYNC = '0';
		-- zacatek vsync
		-- posunout se za hranu
		loop
			tsa := now;
			wait until VGA_VSYNC = '1';
			assert now - tsa = vtpw
				report "pulse width u vsync nesedi" severity failure;
			wait until VGA_VSYNC = '0';
			assert now - tsa = vts
				report "spatna vzdalenost mezi vsync" severity failure;
		end loop;
	end process vsynctbpr;

	-- kontroluje tvar signalu VGA_HSYNC
	-- plati pro nej to same co pro predchozi
	hsynctbpr: process
		variable tsa : time;
	begin
		-- pockat na HSYNC
		wait until VGA_HSYNC = '0';
		-- zacatek hsync
		-- posunout se za hranu
		loop
			tsa := now;
			wait until VGA_HSYNC = '1';
			assert now - tsa = htpw
				report "pulse width u hsync nesedi" severity failure;
			wait until VGA_HSYNC = '0';
			assert now - tsa = hts
				report "spatna vzdalenost mezi hsync" severity failure;
		end loop;
	end process hsynctbpr;


	-- generuje hodinovy signal s frekvenci 50 MHz
	clkpr : process
	begin
		if clk = '1' then
			clk <= '0';
		else
			clk <= '1';
		end if;
		wait for 10 ns;
	end process clkpr;

	-- hlavni testovaci process
  tb : process
	variable row, col : integer;
	variable vstime, expwait : time;
	-- testuje spravnost vykresleni cele obrazovky
	-- vcetne bodu mimo viditelnou cast
	procedure test_screen (rx, ry : integer) is
	begin
		regX <= conv_std_logic_vector(rx, 8);
		regY <= conv_std_logic_vector(ry, 8);
		wait until pixel = 0;
		-- zacatek okna
		-- pro 10 x 20:
		-- horiz: po hsync + tbp + tpix * 10
		-- vert: po vsync + tbp + tline * 20
		assert expected_pixel(rx, ry, pixel) = (VGA_RED, VGA_GREEN, VGA_BLUE)
			report "spatna hodnota pixelu" severity error;
		-- cekej pul pixelu pred testem pro spravnost pixelu
		wait on pixel;
		wait on pixel;
		wait on pixel;
		while pixel > 1  loop
			assert expected_pixel(rx, ry, pixel) = (VGA_RED, VGA_GREEN, VGA_BLUE)
				report "spatna hodnota pixelu" severity error;
			-- cekel pixel, cili 2 pulpixely
			wait on pixel;
			wait on pixel;
		end loop;
		return;
	end procedure;
	
  begin

		-- Wait 100 ns for global reset to finish
		wait for 100 ns;

		-- pevne dane testy
		test_screen(0, 0);
		test_screen(1, 0);
		test_screen(0, 1);
		test_screen(1, 1);
		test_screen(0, 255);
		test_screen(255, 0);
		test_screen(255, 255);
		test_screen(128, 127);
		test_screen(255, 128);
		test_screen(127, 255);
		-- nahodne testy
		for i in 1 to 10 loop
			rnd_num(row);
			rnd_num(col);
			test_screen(row, col);
		end loop;

		assert false report "konec simulace" severity note;
		wait; -- will wait forever
	end process;

end architecture;

----------------------------------------------------------------------------------
-- model komponenty VGS
-- model k TB 1. casti 2. zadani 2. ulohy predmetu BI-PNO Z11.
--
----------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity vgs is
  port (
    clk       : in  std_logic; -- 50 MHz              
    regX      : in  std_logic_vector (7 downto 0); -- X
    regY      : in  std_logic_vector (7 downto 0); -- Y
    VGA_RED   : out std_logic;                        
    VGA_GREEN : out std_logic;                        
    VGA_BLUE  : out std_logic;                        
    VGA_HSYNC : out std_logic;                        
    VGA_VSYNC : out std_logic 
  ); 
end vgs;

architecture Behavioral of vgs is

  signal row : integer := 0;
  signal col : integer := 0;

begin

  vsyncpr: process
    constant vts : time := 16.672 ms;
    constant vtdisp : time := 15.36 ms;
    constant vtpw : time := 64 us;
    constant vtfp : time := 320 us;
    constant vtbp : time := 928 us;
  begin
    VGA_VSYNC <= '0';
    wait for vtpw;
    VGA_VSYNC <= '1';
    wait for vtfp;
    wait for vtdisp;
    wait for vtbp;
  end process vsyncpr;

  -- generuje cislo radku, ktery se prave vykresluje
  -- cislo radku jde od 0 do 520
  -- zobrazovane casti orazovky odpovidaji radky 31 - 511
  -- radek obrazovky := row - 31
  -- vykresleni jedne radky trva 32 us
  rowpr: process
	constant hts : time := 32 us;
  begin
	wait for hts;
	if row = 520 then
		row <= 0;
	else
		row <= row + 1;
	end if;
  end process rowpr;
  
  -- generuje signal VGA_HSYNC
  -- zacatek v simulacnim case 0 s
  hsyncpr: process
    constant hts : time := 32 us;
    constant htdisp : time := 25.6 us;
    constant htpw : time := 3.84 us;
    constant htfp : time := 640 ns;
    constant htbp : time := 1.92 us;
  begin
    VGA_HSYNC <= '0';
    wait for htpw;
    VGA_HSYNC <= '1';
    wait for htbp;
    wait for htdisp;
    wait for htfp;
  end process hsyncpr;

  -- generuje cislo sloupce (poradi pixelu v radku)
  -- ktery prave zobrazujeme
  -- zacatek je v simulacnim case 0 s
  -- bezi volne vuci cislu radku
  -- nabyva hodnot 0 - 639, odpovida primo souradnici
  -- na obrazovce
  -- mimo obrazovku ma hodnotu -1
  colincrpr:process
    constant tpix : time := 40 ns;
	 constant tpw : time := 3.84 us;
	 constant tfp : time := 640 ns;
	 constant tbp : time := 1.92 us;
  begin
    col <= -1;
	 wait for tbp + tpw;
	 col <= 0;
	 -- prirazeni je naplanovano, proto je treba
	 -- testovat predchozi hodnotu
	 while col < 639 loop
		wait for tpix;
		col <= col + 1;
	 end loop;
	 col <= -1;
	 wait for tfp;
  end process colincrpr;
  
  -- generuje vystupni signal
  -- porovnava souradnice vstupujici
  -- do entity s aktualnim radkem a sloupcem
  pixshowpr: process (row, col)
  begin
	VGA_RED <= '0';
	VGA_GREEN <= '0';
	VGA_BLUE <= '0';
	if (row > 30 and row < 31 + 480) and col = ("00" & regX) then
		VGA_RED <= '1';
		VGA_GREEN <= '1';
		VGA_BLUE <= '1';
	end if;
	-- 29 radek back porch a 2 radky vsync
	if (row - 31)  = ("0" & regY) and
		col /= -1 then
		VGA_RED <= '1';
		VGA_GREEN <= '1';
		VGA_BLUE <= '1';
	end if;
  end process pixshowpr;

end Behavioral;

